package com.wujunshen.nature;

import com.hankcs.hanlp.corpus.tag.Nature;
import org.apache.lucene.util.AttributeImpl;
import org.apache.lucene.util.AttributeReflector;

/**
 * <p></p>
 * @author wujunshen
 * @version 1.0
 * @since 1.0
 */
public class NatureAttributeImpl extends AttributeImpl implements NatureAttribute, Cloneable {
    private Nature nature = null;

    @Override
    public void clear() {
        this.nature = null;
    }

    @Override
    public void copyTo(AttributeImpl target) {
        NatureAttribute natureAttribute = (NatureAttribute) target;

        natureAttribute.setNature(this.nature);
    }

    @Override
    public Nature nature() {
        return nature;
    }

    public void reflectWith(AttributeReflector reflector) {
        reflector.reflect(NatureAttribute.class, "nature", this.nature.toString());
    }

    @Override
    public void setNature(Nature nature) {
        this.nature = nature;
    }
}
