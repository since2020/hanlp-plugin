package com.wujunshen.update;

import com.wujunshen.exception.MyException;
import com.wujunshen.plugin.MyAnalysisPlugin;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.elasticsearch.env.Environment;

/**
 * @author wujunshen
 * @version 1.0
 * @since 1.0
 */
@Slf4j
public class HotUpdate {
    private static final String FILE_NAME = "hanlp-hot-update.cfg.xml";
    private static final String REMOTE_EXT_DICT = "remote_ext_dict";
    private static final String REMOTE_EXT_STOP = "remote_ext_stopwords";
    private static final String REMOTE_EXT_SYNONYMS = "remote_ext_synonyms";
    private static final ScheduledExecutorService SCHEDULED_THREAD_POOL_EXECUTOR = new ScheduledThreadPoolExecutor(
            1,
            new BasicThreadFactory.Builder()
                    .namingPattern("schedule-pool-%d")
                    .daemon(true)
                    .build());

    private static HotUpdate hotUpdate = null;
    private final Properties properties;

    private HotUpdate(Environment env) {
        try {
            properties = new Properties();
            log.info(System.getProperty("user.dir"));

            Path configFile =
                    env.pluginsFile().resolve(MyAnalysisPlugin.PLUGIN_NAME).resolve(FILE_NAME);

            log.info(configFile.toString());
            properties.loadFromXML(new FileInputStream(configFile.toFile()));

            String usedLocation = getProperty(REMOTE_EXT_DICT);
            String stopLocation = getProperty(REMOTE_EXT_STOP);
            String synonymLocation = getProperty(REMOTE_EXT_SYNONYMS);

            BaseMonitor baseMonitor = null;
            // used words thread
            if (StringUtils.isNotBlank(usedLocation)) {
                baseMonitor = new UsedLocationMonitor(usedLocation.trim());
            }

            // stop words thread
            if (StringUtils.isNotBlank(stopLocation)) {
                baseMonitor = new StopLocationMonitor(stopLocation.trim());
            }

            // synonymous thread
            if (StringUtils.isNotBlank(synonymLocation)) {
                baseMonitor = new SynonymLocationMonitor(synonymLocation.trim());
            }

            SCHEDULED_THREAD_POOL_EXECUTOR.scheduleAtFixedRate(
                    Objects.requireNonNull(baseMonitor), 10, 60, TimeUnit.SECONDS);
        } catch (IOException e) {
            throw new MyException("hanlp-hot-update.cfg.xml read fail.", e);
        }
    }

    public static synchronized void begin(Environment env) {
        if (hotUpdate == null) {
            hotUpdate = new HotUpdate(env);
        }
    }

    private String getProperty(String key) {
        if (properties != null) {
            return properties.getProperty(key);
        }

        return null;
    }
}
