package com.wujunshen.core;

import com.wujunshen.enumation.SegmentationType;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Tokenizer;

/**
 * @author wujunshen
 * @version 1.0
 * @since 1.0
 */
@Slf4j
@AllArgsConstructor
public class MyAnalyzer extends Analyzer {
    private final SegmentationType segmentationType;

    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
        Tokenizer tokenizer = new MyTokenizer(segmentationType);

        return new TokenStreamComponents(tokenizer);
    }
}
