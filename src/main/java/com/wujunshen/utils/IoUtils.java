package com.wujunshen.utils;

import com.wujunshen.exception.MyException;
import java.io.Closeable;
import java.io.IOException;
import lombok.NoArgsConstructor;

/**
 * @author wujunshen
 * @version 1.0
 * @since 1.0
 */
@NoArgsConstructor
public final class IoUtils {
    public static void close(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (IOException e) {
            throw new MyException("stream close exception.", e);
        }
    }
}
