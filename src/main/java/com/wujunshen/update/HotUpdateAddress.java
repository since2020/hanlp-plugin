package com.wujunshen.update;

import com.wujunshen.enumation.RequestMethod;
import com.wujunshen.utils.Address;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wujunshen
 * @version 1.0
 * @since 1.0
 */
@Getter
@AllArgsConstructor
public class HotUpdateAddress implements Address {
    private String domain;
    private String mapping;
    private RequestMethod method;
}
