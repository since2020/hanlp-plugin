package com.wujunshen.utils;

import com.wujunshen.enumation.RequestMethod;

/**
 * @author wujunshen
 * @version 1.0
 * @since 1.0
 */
public interface Address {

    /**
     * 获取domain
     * @return domain
     */
    String getDomain();

    /**
     * 获取mapping
     * @return mapping
     */
    String getMapping();

    /**
     * 获取method
     * @return method
     */
    RequestMethod getMethod();
}
