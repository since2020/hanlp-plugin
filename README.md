# 0. 写在前面

项目配置

JAVA GraalVM 17

ElasticSearch 8.3.3

Junit5 5.9.0

lombok 1.8.24

logback 1.2.11

hanlp汉语自然语言处理工具包 1.8.3

# 1. 如何使用

## 1.1 获取hanlp语料

直接下载[data.zip](http://nlp.hankcs.com/download.php?file=data)

后续会使用到

## 1.2 本地搭建nginx网站显示静态内容

### 1.2.1 快速安装nginx

以我的mac为例

``` java
brew install nginx
```

然后根据安装成功的提示去找nginx.conf文件，我这里提示如下图

![安装结果](.readme_images/967de8ea.png)

### 1.2.2 配置nginx

根据上图里提示去编辑nginx.conf文件

``` java
vi /opt/homebrew/etc/nginx/nginx.conf
```

编辑内容可以下面文件内容为准，当然读者也可以根据自己情况修改

``` java
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;

    sendfile        on;
    keepalive_timeout  65;

    server {
        listen       8080;
        server_name  local.wujunshen.com;

        location / {
            root   /usr/local/var/www;
            index  index.html index.htm;
        }

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }

    include servers/*;
}
```

其中特别注意的是下面这段

``` java
    listen       8080;
    server_name  local.wujunshen.com;

    location / {
       root   /usr/local/var/www;
       index  index.html index.htm;
    }
```

首先我自定义了一个`local.wujunshen.com`
本地域名，并且和127.0.0.1绑定在一起。然后我把前述的data.zip解压到`/usr/local/var/www`目录,并让root指向它

> 提示
> `local.wujunshen.com`绑定127.0.0.1是这样的
> 我先 `vi /etc/hosts`，打开hosts文件
> 然后copy  `127.0.0.1 local.wujunshen.com`这一行到任意位置
> 最后 `source /etc/hosts`使其生效
> 这样就绑定成功了

编辑nginx.conf完成后，执行

```  java
brew services restart nginx
```

重启nginx,让其生效

### 1.2.3 静态网站内容验证

打开浏览器输入 `http://local.wujunshen.com:8080/`

看见下列界面，证明nginx安装成功

![nginx首页](.readme_images/8890d509.png)

然后接着输入 `http://local.wujunshen.com:8080/data/dictionary/custom/CustomDictionary.txt` 显示如下界面

![语料](.readme_images/6934f78b.png)

这表明nginx网站已经能显示静态内容了

## 1.3 插件安装

### 1.3.1 打包代码

执行

```  java
mvn clean package
```

> 注意
> 代码中的hanlp-hot-update.cfg.xml文件内容
> ```  xml
> <?xml version="1.0" encoding="UTF-8"?>
> <!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
> <properties>
>     <comment>HanLP 扩展配置</comment>
>     <!--用户可以在这里配置远程扩展字典 -->
>     <entry key="remote_ext_dict">
> http://local.wujunshen.com:8080/data/dictionary/custom/CustomDictionary.txt
>     </entry>
>     <!--用户可以在这里配置远程扩展停止词字典-->
>     <entry key="remote_ext_stopwords">
> http://local.wujunshen.com:8080/data/dictionary/stopwords.txt
>     </entry>
>     <!--用户可以在这里配置远程同义词字典-->
>     <entry key="remote_ext_synonyms">
> http://local.wujunshen.com:8080/data/dictionary/synonym/CoreSynonym.txt
>     </entry>
> </properties>
> ```
> 其中`remote_ext_dict`,`remote_ext_stopwords`和`remote_ext_synonyms`都是前述搭建的nginx网站静态内容

### 1.3.2 发布插件

将打包成zip格式的插件包(在/target/releases目录下)

解压到ElasticSearch下的plugins子目录下，这样就发布完成了

### 1.3.3 运行插件

重新启动ElasticSearch

在ElasticSearch目录下的bin子目录启动ElasticSearch

> 注意
>
> 不能用root账号启动
>
> 需要新建账号并赋权，然后启动ElasticSearch

## 1.4 查看插件执行结果

使用head插件，在“复合查询”里输入如下截图内容(事先创建了一个叫`index-test`的索引)
，并按下方“提交请求”按钮执行，可见右侧执行结果

![插件执行结果](.readme_images/0b592194.png)

也可在命令窗口输入下列命令执行，结果和上图是一样的。

```  java
curl -H "Content-Type:application/json" -X POST -d '{
"analyzer": "hanlp_synonym",
"text": "英特纳雄耐尔"
}' http://localhost:9200/index-test/_analyze?pretty=true
```  

见下图命令行执行结果

![命令行执行结果](.readme_images/76906b5b.png)
