package com.wujunshen.plugin;

import com.wujunshen.core.MyAnalyzer;
import com.wujunshen.enumation.SegmentationType;
import com.wujunshen.update.HotUpdate;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.env.Environment;
import org.elasticsearch.index.analysis.AbstractIndexAnalyzerProvider;

/**
 * <p></p>
 * @author wujunshen
 * @version 1.0
 * @since 1.0
 */
public class MyAnalyzerProvider extends AbstractIndexAnalyzerProvider<MyAnalyzer> {
    private final SegmentationType segmentationType;

    public MyAnalyzerProvider(Environment env, String name, Settings settings, SegmentationType segmentationType) {
        super(name, settings);
        this.segmentationType = segmentationType;
        HotUpdate.begin(env);
    }

    @Override
    public MyAnalyzer get() {
        return new MyAnalyzer(segmentationType);
    }
}
