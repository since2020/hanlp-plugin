package com.wujunshen.update;

import com.hankcs.hanlp.dictionary.CustomDictionary;
import java.io.BufferedReader;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2022/8/24 00:00<br>
 */
@Slf4j
public class UsedLocationMonitor extends BaseMonitor {
    UsedLocationMonitor(String location) {
        super(location);
    }

    /**
     * 处理响应
     * @param reader BufferedReader类
     * @throws IOException IO异常
     */
    @Override
    void processResponse(BufferedReader reader) throws IOException {
        String line;

        while ((line = reader.readLine()) != null) {
            String[] message = line.split(",");

            if (message.length == 2) {
                CustomDictionary.add(message[0], message[1]);
            } else {
                CustomDictionary.add(line);
            }

            log.info(line);
        }
    }
}
