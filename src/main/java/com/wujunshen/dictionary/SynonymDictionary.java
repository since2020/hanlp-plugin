package com.wujunshen.dictionary;

import com.hankcs.hanlp.collection.trie.DoubleArrayTrie;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wujunshen
 * @version 1.0
 * @since 1.0
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SynonymDictionary {
    private static DoubleArrayTrie<Set<String>> customerTrie;

    public static synchronized void reloadCustomerSynonyms(Map<String, Set<String>> treeMap) {
        customerTrie = new DoubleArrayTrie<>();
        customerTrie.build((TreeMap<String, Set<String>>) treeMap);
    }

    public static Set<String> get(String key) {
        Set<String> result = new HashSet<>();

        result.add(key);

        if (customerTrie == null) {
            return result;
        }

        return customerTrie.containsKey(key) ? customerTrie.get(key) : result;
    }
}
