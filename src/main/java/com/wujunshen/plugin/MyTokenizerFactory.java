package com.wujunshen.plugin;

import com.wujunshen.core.MyTokenizer;
import com.wujunshen.enumation.SegmentationType;
import com.wujunshen.update.HotUpdate;
import org.apache.lucene.analysis.Tokenizer;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.env.Environment;
import org.elasticsearch.index.IndexSettings;
import org.elasticsearch.index.analysis.AbstractTokenizerFactory;

/**
 * @author wujunshen
 * @version 1.0
 * @since 1.0
 */
public class MyTokenizerFactory extends AbstractTokenizerFactory {
    private final SegmentationType segmentationType;

    public MyTokenizerFactory(
            IndexSettings indexSettings,
            Environment environment,
            String name,
            Settings settings,
            SegmentationType segmentationType) {
        super(indexSettings, settings, name);
        this.segmentationType = segmentationType;
        HotUpdate.begin(environment);
    }

    @Override
    public Tokenizer create() {
        return new MyTokenizer(segmentationType);
    }
}
