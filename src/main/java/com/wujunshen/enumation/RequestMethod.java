package com.wujunshen.enumation;

/**
 * @author wujunshen
 * @version 1.0
 * @since 1.0
 */
public enum RequestMethod {
    /**
     * GET
     */
    GET,
    /**
     * POST
     */
    POST,
    /**
     * PUT
     */
    PUT,
    /**
     * DELETE
     */
    DELETE,
    /**
     * HEAD
     */
    HEAD
}
