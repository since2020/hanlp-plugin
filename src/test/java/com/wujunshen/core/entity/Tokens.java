package com.wujunshen.core.entity;

import java.util.List;
import lombok.Data;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2022/8/24 14:31<br>
 */
@Data
public class Tokens {
    private List<Token> tokens;
}
