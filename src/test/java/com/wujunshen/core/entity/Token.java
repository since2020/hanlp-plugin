package com.wujunshen.core.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2022/8/24 14:06<br>
 */
@Data
@JsonPropertyOrder({"token", "start_offset", "end_offset", "type", "position"})
public class Token {
    private String token;

    @JsonProperty("start_offset")
    private int startOffset;

    @JsonProperty("end_offset")
    private int endOffset;

    private String type;
    private int position;
}
