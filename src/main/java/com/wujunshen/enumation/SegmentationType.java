package com.wujunshen.enumation;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wujunshen
 * @version 1.0
 * @since 1.0
 */
@Getter
@AllArgsConstructor
public enum SegmentationType {
    /**
     * 索引分词
     */
    INDEX("hanlp_index"),
    /**
     * nlp分词
     */
    SEARCH("hanlp_search"),
    /**
     * 同义词索引分词
     */
    SYNONYM("hanlp_synonym");

    private final String title;
}
