package com.wujunshen.plugin;

import com.wujunshen.enumation.SegmentationType;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.lucene.analysis.Analyzer;
import org.elasticsearch.index.analysis.AnalyzerProvider;
import org.elasticsearch.index.analysis.TokenizerFactory;
import org.elasticsearch.indices.analysis.AnalysisModule;
import org.elasticsearch.plugins.AnalysisPlugin;
import org.elasticsearch.plugins.Plugin;

/**
 * @author wujunshen
 * @version 1.0
 * @since 1.0
 */
public class MyAnalysisPlugin extends Plugin implements AnalysisPlugin {
    public static final String PLUGIN_NAME = "hanlp-analyzer";

    @Override
    public Map<String, AnalysisModule.AnalysisProvider<AnalyzerProvider<? extends Analyzer>>> getAnalyzers() {
        Map<String, AnalysisModule.AnalysisProvider<AnalyzerProvider<? extends Analyzer>>> result =
                new HashMap<>(SegmentationType.values().length);

        Arrays.stream(SegmentationType.values())
                .forEach(element -> result.put(
                        element.getTitle(),
                        (indexSettings, environment, s, settings) ->
                                new MyAnalyzerProvider(environment, s, settings, element)));

        return result;
    }

    @Override
    public Map<String, AnalysisModule.AnalysisProvider<TokenizerFactory>> getTokenizers() {
        Map<String, AnalysisModule.AnalysisProvider<TokenizerFactory>> result =
                new HashMap<>(SegmentationType.values().length);

        Arrays.stream(SegmentationType.values())
                .forEach(segmentationType -> result.put(
                        segmentationType.getTitle(),
                        (indexSettings, environment, name, settings) ->
                                new MyTokenizerFactory(indexSettings, environment, name, settings, segmentationType)));

        return result;
    }
}
