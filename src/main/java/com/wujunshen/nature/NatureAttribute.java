package com.wujunshen.nature;

import com.hankcs.hanlp.corpus.tag.Nature;
import org.apache.lucene.util.Attribute;

/**
 * @author wujunshen
 * @version 1.0
 * @since 1.0
 */
public interface NatureAttribute extends Attribute {

    /**
     * 获取Nature
     * @return Nature
     */
    Nature nature();

    /**
     * 设置nature
     * @param nature nature
     */
    void setNature(Nature nature);
}
