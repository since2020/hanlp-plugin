package com.wujunshen.update;

import com.hankcs.hanlp.dictionary.CustomDictionary;
import java.io.BufferedReader;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2022/8/24 00:04<br>
 */
@Slf4j
public class StopLocationMonitor extends BaseMonitor {
    StopLocationMonitor(String location) {
        super(location);
    }

    /**
     * 处理响应
     * @param reader BufferedReader类
     * @throws IOException IO异常
     */
    @Override
    void processResponse(BufferedReader reader) throws IOException {
        String line;

        while ((line = reader.readLine()) != null) {
            CustomDictionary.remove(line);
            log.info(line);
        }
    }
}
