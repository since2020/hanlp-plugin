package com.wujunshen.core;

import com.hankcs.hanlp.seg.common.Term;
import java.util.Objects;

/**
 * @author wujunshen
 */
public class MyTerm extends Term {
    public MyTerm(Term term) {
        super(term.word, term.nature);
        this.offset = term.offset;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MyTerm term) {
            return Objects.equals(this.offset, term.offset) && this.word.equals(term.word);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return this.word.hashCode() + this.offset;
    }
}
