package com.wujunshen.core;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.wujunshen.core.entity.Token;
import com.wujunshen.core.entity.Tokens;
import com.wujunshen.enumation.SegmentationType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2022/8/22 15:28<br>
 */
@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MyAnalyzerTest {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final ExecutorService FIXED_THREAD_POOL_EXECUTOR =
            new ThreadPoolExecutor(5, 5, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
    private static String analyzeText;

    @BeforeAll
    void setUp() {
        analyzeText =
                """
            第一章在从某国太空基地回来之后，足足有两个月的时间，我在家中过的，几乎是足不出户的生活。没有人知道我在家中，都只当我还在外地。
                我除了几个最亲近的人之外，也不和任何人发生联络，所以能够过着没有人打扰的生活。
                但是这样的日子，究竟是不能长期维持的，它因为一个朋友，远自埃及寄来的信而打破了。
                我的那位朋友姓王，是一位有着极高深造诣水利工程师。他是应埃及政府之聘，从荷兰到那里，参加一项极其宏伟的水利建设工程的。
                这项工程，据他形容，可以称的上是世界上最大的水利工程之一，有一座古庙，甚至要整个地迁移。
                而他就是在迁移那座古庙的时候，发现那只箱子，而将之交给我的。
                这是一只十分神秘的箱子，我有必要先将它的外形，形容一番。
                箱子是黄铜铸成的。箱盖和箱子的合缝处，刚好是整个箱子高度的一半，而要打开这只箱子，却绝不是容易的事。
                因为那箱子的锁，是属于十分精巧而且奥妙的一种古锁。我敢断言，如今虽然科学昌明，但是要造出那样的锁来，却不容易。、那锁的情形是这样的：在箱子面上，共分出上百格小格子，而有九十九块小铜片，被嵌在那一百格小格子中，可以自由推动。当然，推动的小格子只有一个空格，可缈为转圈的余地。
                而在九十九小铜片上，都浮雕着一些图案，如果小铜片是按着准确的次序排列起来，那么这些小铜片上凌乱的图案，是可以成为一整幅图画的。
                我的那位朋友，他也相信，如果有耐心地推动那些铜片使他们得到原来的次序，那么，整幅图画重现，那箱子也就可以被打开来了。
                他知道我喜欢稀奇古怪的东西，所以不远万里，将这只箱子寄到了我的手中。
                当这只沉重的铜箱子，到达我手中的时候，我的确大感兴趣，在这箱子上沉缅了几天，但是我随即放弃了，因为我发觉那几乎是不可能的第一，原来的整幅浮雕，究竟是什么，我根本不知道，使我在拼凑之际，绝无依据。
                第二，那九十九块铜片，并不是可以自由取出来，而是只能利用那唯一的空格，作为转圜的余地，所以，要使其中的一片，和另一片拼凑在一起，便要经过极其繁复的手续。
                而铜片一共有九十九片之多，我有什么法子使它们一一回到原来的位置上去？
                我在放弃拼凑那些铜片之后，对这只铜箱子，曾作过细心的观察。
                在那只铜箱子的其它五面，都有着浮雕，人像、兽像都有，线条浑厚拙朴，但是却都不是属于古埃及的艺术范畴的，而是另具风格的一种，看来有些像是印地安人的艺术作品。
                在两侧，有两只铜环。铜环上还铸着一些文字，那些文字，更不是埃及古代的文字。
                我打了一封长长的电报，给那位朋友，告诉他我对这只箱子，感到极大的兴趣，但是我却没有法子将之打开来，是否可以用机械的力量，将之打开，以看一看这只不应该属于埃及，但是却在埃及的古庙之中所发现的铜箱里，究竟有些什么，我并且请他叙述那只箱子发现的经过。
                我的电报是上午打出的，傍晚，我就收到了他的回电，他的回电如此道："卫，我反对将箱子用机械的力量打开，这只箱子，可能造成已经有几千年了，难道我们的智力还不及古人？你可以将这只箱子给我的弟弟，他是学数学的，或许他算得出我们可以打开这只箱子的或然率是多少。他的电话是……。至于这只箱子发现的经过，那是一个过于曲折的故事了，容后再叙。王浚"王俊就是我这位朋友的名字，他是出名慢性子的人，我给他那封电报的最后一句话，弄得心中痒痒的，因为连他都说是一个"十分曲折的故事"，那么这件事的经过，一定十分动人了。
                而事情又是发生在古国埃及，这就使人更觉得它的神秘了。
                我急于想知道他是如何得到那只箱子的愿望，竟超过了打开那只箱子的兴趣。我立即又请他将事情的始末告诉我。并且告诉他，我正闷得发慌，希望他的故事，能使我解闷。
                同时，我和王俊的弟弟王彦，通了一个电话，王彦是在一间高等学校中工作的，他接到了我的电话之后，答应有空就来。
                晚上九点钟，我正在查阅埃及古代铸铜艺术成就的资料，发觉我的料断不错，那铜箱上的浮雕，和埃及艺术绝无共通之点的时候，接着，老蔡带着王彦进来了。
                王彦大约二十六七岁年纪，面色很白，但身体还是健康的，他年纪虽然还轻，但是却有着科学家的风度，他和我是初次见面，十分客气，而且显得有些拘谨。
                我将那只铜箱子的事情和他说了，他谦虚地笑了上一笑，道："我只怕也打不开。"我拍了拍他的肩头，道："打不开也不要紧，你只当是业余的消遣好了。"王彦和我两入，将这只铜箱子抬上了他的车子，他和我挥手告别而去。
                以后的七八天中，王彦也没有和我通电话，我因为等不到王俊的来信，渐渐地也将这件事情淡忘了。
                那一天晚上，大约是在给王彦将箱子取走之后的第十天，那是一个回南天，空气湿得反常，使人觉得十分不舒服。
                中午，我正在假寐，床头的电话，突然响了起来。
                说起来十分奇怪，电话的铃响声，次次都是一样的。但是有时候，人会直觉地觉出，电话铃响得十分急，像是在预告有要紧的事情一样。
                我立即拿起了话筒。
                从电话中传来的，是王彦的声音。
                他的呼吸有点急促，道："是卫斯理先生么？我……我是王彦。"我道："是的，有什么事，不妨慢慢他说。"我听得出他长长地吸了一口气，道："我……已经将那箱子面上的丸十九块铜片，排列成了一幅浮雕画了，"我从床上跳了起来道："祝你成功，那你已经打开箱子了。"王彦道："还没有打开，但是我忽然有一种奇妙的预感，觉得打开箱子，会对我不利。"我"哈哈"大笑了起来，道："你大概受了埃及古代咒语会灵验的影响，我可以告诉你，这箱子虽然在埃及古庙中被发现，但是绝不是埃及的东西。"王彦又问道："其他古民族，难道就没有咒语么？"我又笑了起来，道："我以为学数学的人，多是枯燥乏味的，但是你却有着丰富的想象力！"王彦在那边不好意思地笑了笑，道："好，我打开箱子之后，再和你通电话。"我放下了话筒，将枕头拉高些，垫住了背部，舒服地躺了下来。我想，大约等上十分钟。
                就可以得到王彦的电话了。
                可是，我抽了七八支烟，已经过去了将近一个小时了，王彦仍然没有打电话来。
                我忍不住拨了他的电话号码，可是那边却没有人接听，电话公司又说王彦的电话并没有损坏。
                我党出事情有些不妙；但是我却绝不相信王彦会遇到什么意外，因为他只不过是打开一只古代的铜箱子而已！
                但是，时间一点一点地过去，我早已从床上跳了起来，在室中来回地踱着步，主彦为什么隔了那么久时间，仍然不打电话来通知我箱子之中究竟有些什么东西呢？如果他打不开那只箱子的诸，也可以给我一个电话的，在我的印象之中。
                王彦绝不是做事有头无尾的人！
                然而，当我第十几次地又忍不住再打的话给他，而他那方面，仍然没有人接听电话之际，已经是黄昏时分了。
                从王彦打电话通知我，说他已成功地拼凑起了那铜箱子面上的图画起，到如今已有将近五个小时了！这五个小时之中，音讯全无，王彦究竟是发生了什么事情呢？
                虽然我想来想去，王彦没有遭到什么意外的可能，但是我却不能不为他耽心。
                他的哥哥给了我他的电话号码，而上次王彦来的时候，他也未曾告诉我他的地址，所以，当我等得实在不耐烦时，我又拿起了电话，请我一个当私家侦探的朋友帮忙。
                那位朋友和他的助手，曾经以极长的时间，自己编了一本电话簿，是从电话号码来查那个电话的地址的。不到五分钟，我已经得到了我所要的地址，王彦住在碧仙道三号四楼。
                我知道碧仙道是高尚的住宅，正适合王彦的身份，我放下了话筒，已准备按址去找他。
                但是，我刚到门口，电话铃声，这然大作。
                我连忙跳到了电话之旁，一把拿起了话筒。一拿起话筒来，我便听到了王彦浓重的喘息声。
                我更加觉得事情十分不寻常，我连忙问："什么事情？发生了什么事？"王彦的喘气声，越来越是浓重，像是他的身上，正负着千斤重压一样。我一连问了七八声，才听得他的讲话声音，道，"我……我遭到了一些麻烦，我可以来看你吗，立即来！"我听出王彦虽然还在说"遭到了一些麻烦"但实质上，他却一定遭受到了极大的困扰！他给我的印象，是十分镇定和有条理的人，但这时：从电话中听来，他的镇定和有条理，似乎都破坏无遗了。
                我不加考虑，道："好，你立即就来。"
                王彦并没有多说什么。"拍"地一声，便挂断了电话，我手拿着听筒，呆了一会，才放了下去，我感到，一个十分巨大的变故，正在王彦的身上发生，那种变故是因什么而起的呢？
                难道就是固为那只不应该属于埃及，但是却在埃及古庙中发现的箱子么？
                碧仙道离我的住处，并不十分远，在我算来，至多有十分钟，王彦便可以来了，但是我却足足等了二十分钟，才听到门铃声。
                一听到门铃声，我立即奔下楼去，同时也听得老蔡在粗声粗气地问道："什么人？你找谁？"我连忙道："老蔡，他就是上次来过的王先生，你快开门让他进来。"老蔡的眼睛，一直凑在大门上的望人镜上，听得我这样说法，他转过头来，面上现出奇怪的神色，道："他就是上次来过的王先生？"老蔡平时绝不是这样罗嗦的人，我不禁不耐烦起来，道："你快开门吧。"老蔡不敢多出声，将门打了开来，一个人自门外，向内跨了一步，我抬头看去，也不禁一呆！
                这是王彦么？
                难怪老蔡刚才向我望来之际，面上充满着犹豫的神色了，因为连我也不敢肯定，这时出现在我家门口的人，是不是王彦！
                那人的身材，和王彦相同，但是由于他穿着大衣，一对大衣领高高地竖起，手上戴着手套，头上戴着帽子，将一条围巾，裹住了他整个脸，而且，还戴上一副很大的黑眼镜！
                他这身打扮，即使到爱斯基摩人家中去作客，也不必害怕冻死了，更何况今天还是一个因南天，天气懊湿，我只不过穿着一件衬衫而已：我呆了一呆间，已听得王彦的声音，透过了包在他脸上的围巾中而传了出来，声音虽然显得不清楚，但是我仍然可以肯定，那正是王彦的声音，也就是说，站在我面前的人，正是王彦。
                王彦的声音很急促，道："你……等了我很久了么？"我向前连跨了几步，道："你可是不舒服么？"王彦发出了一声音笑，道："不舒服，不，不，我很好。"他显然是在说谎，绝对不会有一个"很好"的人，作出这种打扮来的。我望着他，道："刚才你在电话中说你有麻烦，那是什么？"王彦打横走开了几步，他像是有意要离得我远一些一样，在一张沙发上坐了下来，却并不出声。
                我越来越觉得事情十分怪异，向他走近了几步，追问道："什么事使你心中不安？你是怕冷么？为什么不将帽子，眼镜除下来？"王彦立即站了起来，颤声道："除下来？
                不！不！"他一面说，一面乱摇手。
                我和王彦，并不能算是很熟的朋友，所以他不肯除下帽子，眼镜以及一切他遮掩脸面身子的东西，我也不便过份勉强他。我只是道："你来找我，当然是想得到我的帮助了？"王彦道："是的，我想问你一些事情。"我作了个无可奈何的手势，道："好，那你就说吧！"王彦的呼吸，又急促了起来，道："那只……那只黄铜箱子……是怎么得来的？"事情果然和那只箱子有关——我心中迅速地想着，而同时，我也立即口答王彦："那是你哥哥从埃及寄来给我的。"王彦神经质地挥着手，道："不！不！我的意思是问，我哥哥是从什么地方，怎样得到这只箱子的，那箱子的来历，究竟怎样？"我虽然没有法子看到王彦的脸面，也无从知道他面上的神色如何？
                但是从他的行动、言语之中，我却可以看出他的神经，是处在极度紧张，近乎失常的状态之中，我顾不得答他的问题，只是追问道："那只箱子怎么样？你不是打开了它么？它给了你什么困扰？"王彦并不回答我，他只是尖声地，带着哭音地叫道："告诉我，告诉我那箱子的来源！"我叹了一口气，道："我没有法子告诉你，你哥哥只说，他得到那只箱子，有一个十分曲折的故事，我打了两封长电去询问，但是他却并没有口答给我！"王彦刚才，在急切地向我询问之际，身子前俯，半站半坐，这时，听到了我给他这样的回答，他又颓然地坐倒在沙发之上，喃喃地道、"那么……我……我……"他一面在喃喃自语，一面身子竟在激烈地发着颤。我连忙道："王彦，你身子一定不舒服，你可要我召唤医生么？"王彦霍地站了起来，道："不，不用了。我……我该告辞了。"他一面说，一面面对着我，向门口退去，我自然不肯就这样让他离去。因为我心中的疑团，不但没有得到任何解释，而且还因王彦的怪举动而更甚了。
                我向他迎了上去，王彦双手乱摇，道："你……你不必送了，我自己会走的。"他双手戴着厚厚的手套，在那样暖和的天气，他为什么要戴手套呢？
                我一面想着，一面道："你到我这里来，不见得就是为了要问我这样几句话吧。"王彦道："不是……不是……是的……就是问这几句话。"他显然已到了语无伦次的程度，我更不能就这样放他离去！
                王彦仍在不断地后退，在他将要退到门口之际，我猛地一跃，向前跃出了三四步，到了他的身前，一伸手。已经握住了他右手的手套，道："这么热的天，你为什么将自己装在'套子'里？"王彦这时的袋束，和契坷夫笔下的那个"装在套子里的人"十分相似，所以我才这样说法；的。由此可见，我在那样说法之际，虽然觉得事情十分费疑猜，但却还不以为事情是十分严重的，要不然我也不会那样轻松了。
                我的行动，显然是完全出于王彦的意料之外的，我一握住他右手手套，立即一拉，将他右手的手套拉脱，而王彦在那时候，双手仍在乱遥要阻止我接近他。
                然而，在不到十分之一秒的时间内，我和王彦两入，都僵住了不动。
                在刹那间，我如同遭受雷击一样！
                我看到王彦的双手，仍然在摆出挡驾的姿势，他的左手，还戴着手套，但是右手的手套，已被我除了下来，他的右手，在被我除下了手套之后……唉，我该怎么说才好呢？
                我看到的，并不是一只手——当然那是一只手，但是却是没有血，没有肉的，只不过是五根指骨头。
                我所看到的，是一副手骨""";
    }

    @AfterAll
    void tearDown() {
        analyzeText = null;
    }

    @DisplayName("索引分词")
    @Order(0)
    @Test
    void testAnalyzeIndexText() throws IOException, InterruptedException {
        List<Token> resultList = analyze(SegmentationType.INDEX, analyzeText);

        assertThat(resultList, notNullValue());
        assertThat(resultList.size(), is(4155));
    }

    @DisplayName("nlp分词")
    @Order(10)
    @Test
    void testAnalyzeSearchText() throws IOException, InterruptedException {
        List<Token> resultList = analyze(SegmentationType.SEARCH, analyzeText);

        assertThat(resultList, notNullValue());
        assertThat(resultList.size(), is(3870));
    }

    @DisplayName("同义词索引分词")
    @Order(20)
    @Test
    void testAnalyzeSynonymText() throws IOException, InterruptedException {
        List<Token> resultList = analyze(SegmentationType.SYNONYM, analyzeText);

        assertThat(resultList, notNullValue());
        assertThat(resultList.size(), is(4155));
    }

    @DisplayName("异步分词试验")
    @Order(30)
    @Test
    void testAsyncAnalyzeText() {
        // 创建异步执行任务:
        CompletableFuture<List<Token>> completableFuture = CompletableFuture.supplyAsync(
                        () -> {
                            try {
                                return analyze(SegmentationType.SYNONYM, analyzeText);
                            } catch (IOException | InterruptedException e) {
                                e.printStackTrace();
                                throw new RuntimeException(e);
                            }
                        },
                        FIXED_THREAD_POOL_EXECUTOR)
                // 如果执行异常
                .exceptionally((e) -> {
                    e.printStackTrace();
                    return null;
                });

        // 如果执行成功:
        completableFuture.thenAccept((result) -> {
            log.info("{} \nis ok", result);
        });
    }

    private List<Token> analyze(SegmentationType segmentationType, String text)
            throws IOException, InterruptedException {
        Tokens result = new Tokens();
        List<Token> resultList = new ArrayList<>();
        Analyzer analyzer = new MyAnalyzer(segmentationType);
        TokenStream tokenStream = analyzer.tokenStream("text", text);

        tokenStream.reset();

        while (tokenStream.incrementToken()) {
            CharTermAttribute charTermAttribute = tokenStream.getAttribute(CharTermAttribute.class);
            TypeAttribute typeAttribute = tokenStream.getAttribute(TypeAttribute.class);

            OffsetAttribute offsetAttribute = tokenStream.getAttribute(OffsetAttribute.class);

            PositionIncrementAttribute positionIncrementAttribute =
                    tokenStream.getAttribute(PositionIncrementAttribute.class);

            Token token = new Token();
            token.setToken(charTermAttribute.toString());
            token.setStartOffset(offsetAttribute.startOffset());
            token.setEndOffset(offsetAttribute.endOffset());
            token.setType(typeAttribute.type());
            token.setPosition(positionIncrementAttribute.getPositionIncrement());

            resultList.add(token);
        }

        tokenStream.close();

        result.setTokens(resultList);

        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        log.info("{}\n", objectMapper.writeValueAsString(result));

        return resultList;
    }
}
